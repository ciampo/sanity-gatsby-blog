export default {
  widgets: [
    { name: 'structure-menu' },
    {
      name: 'project-info',
      options: {
        __experimental_before: [
          {
            name: 'netlify',
            options: {
              description:
                'NOTE: Because these sites are static builds, they need to be re-deployed to see the changes when documents are published.',
              sites: [
                {
                  buildHookId: '5e1840d1127a45ab05941551',
                  title: 'Sanity Studio',
                  name: 'sanity-gatsby-blog-studio-oe27mn56',
                  apiId: 'c53c15ba-61c3-4f35-8582-6e0f61ad3f99'
                },
                {
                  buildHookId: '5e1840d1f9b92886619e9277',
                  title: 'Blog Website',
                  name: 'sanity-gatsby-blog-web-ppnqd4xv',
                  apiId: 'bf0f4365-c67f-489e-b772-52b8c685d037'
                }
              ]
            }
          }
        ],
        data: [
          {
            title: 'GitHub repo',
            value: 'https://github.com/ciampo/sanity-gatsby-blog',
            category: 'Code'
          },
          { title: 'Frontend', value: 'https://sanity-gatsby-blog-web-ppnqd4xv.netlify.com', category: 'apps' }
        ]
      }
    },
    { name: 'project-users', layout: { height: 'auto' } },
    {
      name: 'document-list',
      options: { title: 'Recent blog posts', order: '_createdAt desc', types: ['post'] },
      layout: { width: 'medium' }
    }
  ]
}
